<?php declare( strict_types=1 );

namespace Buro26\Sync\Entity;


use Swift\Orm\Attributes\Behavior\CreatedAt;
use Swift\Orm\Attributes\Behavior\UpdatedAt;
use Swift\Orm\Attributes\Entity;
use Swift\Orm\Attributes\Field;
use Swift\Orm\Attributes\Index;
use Swift\Orm\Attributes\Relation\HasOne;
use Swift\Orm\Attributes\Relation\Inverse;
use Swift\Orm\Entity\AbstractEntity;
use Swift\Orm\Mapping\Definition\Relation\EntityRelationType;
use Swift\Orm\Types\FieldTypes;

#[Entity( table: 'sync_item' )]
#[CreatedAt( field: 'created' )]
#[UpdatedAt( field: 'updated' )]
#[Index( fields: [ 'scope', 'tag' ] )]
class SyncItem extends AbstractEntity {
    
    #[Field( name: 'id', primary: true, type: FieldTypes::INT, length: 11 )]
    protected int $id;
    
    #[Field( name: 'reference', type: FieldTypes::STRING, length: 255, empty: false, unique: true )]
    protected string $reference;
    
    #[Field( name: 'scope', type: FieldTypes::STRING, length: 255 )]
    protected string $scope;
    
    #[Field( name: 'tag', type: FieldTypes::STRING, length: 255 )]
    protected string $tag;
    
    #[Field( name: 'created', type: FieldTypes::DATETIME )]
    protected \DateTimeInterface|null $created;
    
    #[Field( name: 'updated', type: FieldTypes::DATETIME )]
    protected \DateTimeInterface|null $updated;
    
    #[Field( name: 'value', type: FieldTypes::JSON )]
    protected \stdClass $value;
    
    #[Field( name: 'state', type: FieldTypes::INT, length: 11, index: true )]
    protected int $state;
    
    #[HasOne( targetEntity: SyncItemOut::class, inverse: new Inverse( as: 'syncItem', type: EntityRelationType::BELONGS_TO ), nullable: true )]
    protected SyncItemOut|null $syncItemOut = null;
    
    public function getId(): int|null {
        return $this->id ?? null;
    }
    
    public function getReference(): string {
        return $this->reference;
    }
    
    public function setReference( string $reference ): self {
        $this->reference = $reference;
        
        return $this;
    }
    
    public function getScope(): string {
        return $this->scope;
    }
    
    public function setScope( string $scope ): self {
        $this->scope = $scope;
        
        return $this;
    }
    
    public function getTag(): string {
        return $this->tag;
    }
    
    public function setTag( string $tag ): self {
        $this->tag = $tag;
        
        return $this;
    }
    
    public function getCreated(): \DateTimeInterface|null {
        return $this->created ?? null;
    }
    
    public function getUpdated(): \DateTimeInterface|null {
        return $this->updated ?? null;
    }
    
    public function getValue(): \stdClass|null {
        return $this->value ?? null;
    }
    
    public function setValue( \stdClass $value ): self {
        $this->value = $value;
        
        return $this;
    }
    
    public function getState(): int|null {
        return $this->state ?? null;
    }
    
    public function setState( int $state ): self {
        $this->state = $state;
        
        return $this;
    }
    
    /**
     * @param \Buro26\Sync\Entity\SyncItemOut|null $syncItemOut
     */
    public function setSyncItemOut( SyncItemOut|null $syncItemOut ): void {
        $this->syncItemOut = $syncItemOut;
    }
    
    public function getSyncItemOut(): SyncItemOut|null {
        return $this->syncItemOut;
    }
    
}