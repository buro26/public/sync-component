<?php declare( strict_types=1 );


namespace Buro26\Sync\Entity;


use Swift\Orm\Attributes\Behavior\CreatedAt;
use Swift\Orm\Attributes\Behavior\UpdatedAt;
use Swift\Orm\Attributes\Entity;
use Swift\Orm\Attributes\Field;
use Swift\Orm\Entity\AbstractEntity;
use Swift\Orm\Types\FieldTypes;

#[Entity( table: 'sync_item_out' )]
#[CreatedAt( field: 'created' )]
#[UpdatedAt( field: 'updated' )]
class SyncItemOut extends AbstractEntity {
    
    #[Field( name: 'id', primary: true, type: FieldTypes::INT, length: 11 )]
    protected int $id;
    
    #[Field( name: 'scope', type: FieldTypes::TEXT, length: 50)]
    protected string $scope;
    
    #[Field( name: 'tag', type: FieldTypes::TEXT, length: 50)]
    protected string $tag;
    
    #[Field( name: 'created', type: FieldTypes::DATETIME )]
    protected \DateTimeInterface|null $created;
    
    #[Field( name: 'updated', type: FieldTypes::DATETIME )]
    protected \DateTimeInterface|null $updated;
    
    protected SyncItem|null $syncItem;
    
    public function getId(): int|null {
        return $this->id ?? null;
    }
    
    /**
     * @return string
     */
    public function getScope(): string {
        return $this->scope;
    }
    
    /**
     * @param string $scope
     */
    public function setScope( string $scope ): void {
        $this->scope = $scope;
    }
    
    /**
     * @return string
     */
    public function getTag(): string {
        return $this->tag;
    }
    
    /**
     * @param string $tag
     */
    public function setTag( string $tag ): void {
        $this->tag = $tag;
    }
    
    public function getCreated(): \DateTimeInterface|null {
        return $this->created ?? null;
    }
    
    public function getUpdated(): \DateTimeInterface|null {
        return $this->updated ?? null;
    }
    
    public function setUpdated( \DateTimeInterface $updated ): void {
        $this->updated = $updated;
    }
    
    public function getSyncItem(): SyncItem|null {
        return $this->syncItem;
    }
    
    public function setSyncItem( SyncItem $syncItem ): void {
        $this->syncItem = $syncItem;
        $this->syncItem->setSyncItemOut( $this );
    }
    
}