<?php declare( strict_types=1 );


namespace Buro26\Sync\Service;


use Swift\DependencyInjection\Attributes\Autowire;

#[Autowire]
final class SyncService {
    
    public function __construct(
        private readonly \Swift\Orm\EntityManagerInterface   $entityManager,
        private readonly \Swift\Logging\AppLogger            $logger,
        private readonly \Buro26\Sync\Console\ConsoleLogger  $consoleLogger,
        private readonly \Buro26\Sync\Handler\HandlerFactory $handlerFactory,
        /** @var \Buro26\Sync\Runner\SyncRunnerInterface[] $syncRunners */
        #[Autowire( tag: 'sync.runner' )]
        private readonly iterable                            $syncRunners,
    ) {
    }
    
    public function run( string|null $scope = null, string|null $tag = null ): void {
        $inHandlers  = $this->handlerFactory->getHandlers( \Buro26\Sync\Handler\HandlerType::IN, $scope, $tag );
        $outHandlers = $this->handlerFactory->getHandlers( \Buro26\Sync\Handler\HandlerType::OUT, $scope, $tag );
        
        if ( empty( $inHandlers ) && empty( $outHandlers ) ) {
            $this->consoleLogger->getIo()->writeln( 'No handlers found for this sync' );
            
            return;
        }
        
        if ( ! empty( $inHandlers ) ) {
            $this->runHandlers( $inHandlers, \Buro26\Sync\Handler\HandlerType::IN );
        }
        if ( ! empty( $outHandlers ) ) {
            $this->runHandlers( $outHandlers, \Buro26\Sync\Handler\HandlerType::OUT );
        }
    }
    
    /**
     * Execute batch of handlers
     *
     * @param \Buro26\Sync\Handler\HandlerInInterface[]|\Buro26\Sync\Handler\HandlerOutInterface[] $handlers
     * @param \Buro26\Sync\Handler\HandlerType                                                     $method
     */
    public function runHandlers( array $handlers, \Buro26\Sync\Handler\HandlerType $method ): void {
        foreach ( $handlers as $handler ) {
            $config = $handler->_getConfig();
            $scope  = method_exists( $config, 'getScope' ) ? $config->getScope() : $config->getOutScope();
            $tag    = method_exists( $config, 'getTag' ) ? $config->getTag() : $config->getOutTag();
            
            foreach ( $this->syncRunners ?? [] as $syncRunner ) {
                try {
                    if ( ! $syncRunner->supports( $method ) ) {
                        continue;
                    }
                    
                    $this->consoleLogger->getIo()->writeln( sprintf( '<fg=blue;options=bold>%s handler: %s</>', $method->name, get_class( $handler ) ) );
                    
                    $syncRunner->run( $handler );
                } catch ( \Buro26\Sync\Exception\HandlerFailedException $exception ) {
                    // Handler error
                    $this->logger->error(
                        sprintf( 'Handler error while running sync: %s', $exception->getMessage() ),
                        [
                            'handler' => get_class( $handler ),
                            'scope'   => $scope,
                            'tag'     => $tag,
                            'context' => $exception->getContext(),
                        ],
                    );
                }
            }
        }
    }
    
    /**
     * @param string   $scope
     * @param string   $tag
     * @param int|null $from from date and time as timestamp
     * @param int|null $to   to date and time as timestamp
     *
     * @return array|null
     * @throws \Exception
     */
    public function getItems( string $scope, string $tag, int|null $from = null, int|null $to = null ): array|null {
        $list = $this->entityManager->findMany(
            \Buro26\Sync\Entity\SyncItem::class,
            [
                'scope' => $scope,
                'tag'   => $tag,
            ],
        );
        
        $response = [
            'creations' => [],
            'updates'   => [],
            'deletions' => [],
        ];
        
        if ( $list->count() < 1 ) {
            return null;
        }
        
        foreach ( $list as $item ) {
            /** @var \Buro26\Sync\Entity\SyncItem $item */
            $itemCreatedTimeStamp = $item->getCreated()?->getTimestamp();
            $itemUpdatedTimeStamp = $item->getUpdated()?->getTimeStamp();
            if ( ( $from !== null ) && ( $itemUpdatedTimeStamp < $from ) ) {
                continue;
            }
            if ( ( $to !== null ) && ( $itemUpdatedTimeStamp >= $to ) ) {
                continue;
            }
            
            $plain          = $item->toObject();
            $plain->created = $item->getCreated()->format( \DateTimeInterface::ATOM );
            $plain->updated = $item->getUpdated()->format( \DateTimeInterface::ATOM );
            
            switch ( $item->getState() ) {
                case 0:
                    $response[ 'deletions' ][] = $plain;
                    break;
                case 1:
                    $response[ ( $itemCreatedTimeStamp >= $from ) ? 'creations' : 'updates' ][] = $plain;
                    break;
            }
        }
        
        return $response;
    }
}
