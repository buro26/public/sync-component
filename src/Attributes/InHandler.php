<?php declare( strict_types=1 );


namespace Buro26\Sync\Attributes;

#[\Attribute(\Attribute::TARGET_CLASS)]
#[\AllowDynamicProperties]
class InHandler {
    
    public function __construct(
        protected readonly string $scope,
        protected readonly string $tag,
    ) {
    }
    
    /**
     * @return string
     */
    public function getScope(): string {
        return $this->scope;
    }
    
    public function getTag(): string {
        return $this->tag;
    }
    
}