<?php declare( strict_types=1 );


namespace Buro26\Sync\Attributes;

#[\Attribute(\Attribute::TARGET_CLASS)]
#[\AllowDynamicProperties]
class OutHandler {
    
    public function __construct(
        protected readonly string $inScope,
        protected readonly string $inTag,
        protected readonly string $outScope,
        protected readonly string $outTag,
    ) {
    }
    
    /**
     * @return string
     */
    public function getInScope(): string {
        return $this->inScope;
    }
    
    /**
     * @return string
     */
    public function getInTag(): string {
        return $this->inTag;
    }
    
    /**
     * @return string
     */
    public function getOutScope(): string {
        return $this->outScope;
    }
    
    /**
     * @return string
     */
    public function getOutTag(): string {
        return $this->outTag;
    }
    
    
    
}