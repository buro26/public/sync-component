<?php declare( strict_types=1 );

namespace Buro26\Sync\GraphQl\Schema\Generator;

use Buro26\Sync\Handler\HandlerType;
use GraphQL\Type\Definition\Type;
use Swift\DependencyInjection\Attributes\Autowire;
use Swift\GraphQl\Schema\Builder\Builder;
use Swift\GraphQl\Schema\Builder\EnumBuilder;
use Swift\GraphQl\Schema\Builder\ObjectBuilder;
use Swift\GraphQl\Schema\Registry;
use Swift\GraphQl\Type\TypeFactory;

#[Autowire]
class SyncLatestGenerator implements \Swift\GraphQl\Schema\Generator\GeneratorInterface {
    
    public function __construct(
        protected readonly \Buro26\Sync\Handler\HandlerFactory $handlerFactory,
    ) {
    }
    
    public function generate( \Swift\GraphQl\Schema\Registry $registry ): \Swift\GraphQl\Schema\Registry {
        $registry->extendType( 'Query', function( ObjectBuilder $objectBuilder ) use ( $registry ) {
            $response  = $this->generateResponse( $registry );
            $scopeEnum = $this->generateScopeEnum( $registry );
            $tagEnum   = $this->generateTagEnum( $registry );
            
            $objectBuilder->addField( 'SyncItemsLatest', [
                'type'        => static fn() => Registry::$typeMap[ $response->getName() ],
                'description' => 'Get changes since provided dates',
                'args'        => [
                    'scope' => [
                        'type' => static fn() => Type::nonNull( Registry::$typeMap[ $scopeEnum->getName() ] ),
                    ],
                    'tag'   => [
                        'type' => static fn() => Type::nonNull( Registry::$typeMap[ $tagEnum->getName() ] ),
                    ],
                    'from'  => [
                        'type' => Type::nonNull( TypeFactory::dateTime() ),
                    ],
                    'to'    => [
                        'type' => Type::nonNull( TypeFactory::dateTime() ),
                    ],
                ],
            ] );
            
            
        } );
        
        return $registry;
    }
    
    protected function generateResponse( Registry $registry ): ObjectBuilder {
        $object = Builder::objectType( 'SyncItemsResponse' )
                         ->setDescription( 'Changes since provided dates' )
                         ->addField( 'creations', Builder::fieldType( 'creations', static fn() => Registry::$typeMap[ 'SyncItemConnection' ] )->buildType() )
                         ->addField( 'updates', Builder::fieldType( 'updates', static fn() => Registry::$typeMap[ 'SyncItemConnection' ] )->buildType() )
                         ->addField( 'deletions', Builder::fieldType( 'deletions', static fn() => Registry::$typeMap[ 'SyncItemConnection' ] )->buildType() );
        $registry->objectType( $object );
        
        return $object;
    }
    
    protected function generateScopeEnum( Registry $registry ): EnumBuilder {
        $object = Builder::enumType( 'SyncItemScope' );
        
        foreach ( $this->handlerFactory->getHandlers( HandlerType::IN ) as $handler ) {
            $config = $handler->_getConfig();
            $object->addValue( $config->getScope(), $config->getScope() );
        }
        
        $registry->enumType( $object );
        
        return $object;
    }
    
    protected function generateTagEnum( Registry $registry ): EnumBuilder {
        $object = Builder::enumType( 'SyncItemTag' );
        
        foreach ( $this->handlerFactory->getHandlers( HandlerType::IN ) as $handler ) {
            $config = $handler->_getConfig();
            $object->addValue( $config->getTag(), $config->getTag() );
        }
        
        $registry->enumType( $object );
        
        return $object;
    }
    
    
}