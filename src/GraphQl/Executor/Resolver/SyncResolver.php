<?php declare( strict_types=1 );


namespace Buro26\Sync\GraphQl\Executor\Resolver;

use GraphQL\Type\Definition\ResolveInfo;
use Swift\DependencyInjection\Attributes\Autowire;
use Swift\GraphQl\Attributes\Resolve;
use Swift\Orm\Dbal\ResultCollection;

#[Autowire]
class SyncResolver extends \Swift\GraphQl\Executor\Resolver\AbstractResolver {
    
    public function __construct(
        protected readonly \Buro26\Sync\Service\SyncService $syncService,
    ) {
    }
    
    #[Resolve( name: 'SyncItemsLatest' )]
    public function resolveLatestSyncItems( $objectValue, $args, $context, ResolveInfo $info ): array {
        /** @var \DateTimeInterface|null $fromValue */
        $fromValue = $args[ 'from' ] ?? null;
        /** @var \DateTimeInterface|null $toValue */
        $toValue = $args[ 'to' ] ?? null;
        
        [ 'creations' => $creations, 'updates' => $updates, 'deletions' => $deletions ] = $this->syncService->getItems(
            $args[ 'scope' ],
            $args[ 'tag' ],
            $fromValue?->getTimestamp(),
            $toValue?->getTimestamp(),
        );
        
        return [
            'creations' => new ResultCollection( $creations ),
            'updates'   => new ResultCollection( $updates ),
            'deletions' => new ResultCollection( $deletions ),
        ];
    }
    
}