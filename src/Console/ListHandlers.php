<?php declare( strict_types=1 );

namespace Buro26\Sync\Console;

use Buro26\Sync\Handler\HandlerFactory;
use Buro26\Sync\Handler\HandlerType;
use Swift\Console\Command\AbstractCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


final class ListHandlers extends AbstractCommand {
    
    public function __construct(
        private readonly HandlerFactory $handlerFactory,
    ) {
        
        parent::__construct();
    }
    
    public function getCommandName(): string {
        return 'sync:handlers:list';
    }
    
    /**
     * Method to set command configuration
     */
    protected function configure(): void {
        parent::configure();
        $this
            ->setDescription( 'List all registered handlers' )
            ->setHelp( 'List all registered handlers' );
    }
    
    protected function execute( InputInterface $input, OutputInterface $output ): int {
        $arr = [];
        
        foreach ( $this->handlerFactory->getHandlers(HandlerType::IN) as $handler ) {
            $arr[]  = [
                $handler->_getConfig()->getScope(),
                $handler->_getConfig()->getTag(),
                'IN',
                $handler::class,
            ];
        }
        foreach ( $this->handlerFactory->getHandlers(HandlerType::OUT) as $handler ) {
            $arr[]  = [
                $handler->_getConfig()->getInScope() . ' => ' . $handler->_getConfig()->getOutScope(),
                $handler->_getConfig()->getInTag() . ' => ' . $handler->_getConfig()->getOutTag(),
                'OUT',
                $handler::class,
            ];
        }
        
        $this->io->title( sprintf('Registered %s handlers', count($arr)) );
        
        $this->io->table(
            [ 'scope', 'tag', 'type', 'FQCN' ],
            $arr,
        );
        
        return Command::SUCCESS;
    }
    
    
}