<?php declare( strict_types=1 );


namespace Buro26\Sync\Console;


class ConsoleLogger {
    
    protected \Symfony\Component\Console\Style\SymfonyStyle|null $io = null;
    private \Swift\Console\Command\AbstractCommand $command;
    
    /**
     * @return \Symfony\Component\Console\Style\SymfonyStyle|null
     */
    public function getIo(): \Symfony\Component\Console\Style\SymfonyStyle|null {
        return $this->io;
    }
    
    /**
     * @param \Symfony\Component\Console\Style\SymfonyStyle $io
     */
    public function setIo( \Symfony\Component\Console\Style\SymfonyStyle $io ): void {
        $this->io = $io;
    }
    
    /**
     * @return \Swift\Console\Command\AbstractCommand
     */
    public function getCommand(): \Swift\Console\Command\AbstractCommand {
        return $this->command;
    }
    
    /**
     * @param \Swift\Console\Command\AbstractCommand $command
     */
    public function setCommand( \Swift\Console\Command\AbstractCommand $command ): void {
        $this->command = $command;
    }
    
}