<?php declare( strict_types=1 );

namespace Buro26\Sync\Console;

use Swift\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Buro26\Sync\Service\SyncService;


final class SyncRunCommand extends AbstractCommand {
    
    public function __construct(
        private readonly SyncService   $syncService,
        private readonly ConsoleLogger $consoleLogger,
    ) {
        
        parent::__construct();
    }
    
    public function getCommandName(): string {
        return 'sync:run';
    }
    
    /**
     * Method to set command configuration
     */
    protected function configure(): void {
        parent::configure();
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription( 'Run sync for all handlers' )
            
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp( 'This command will run all sync handlers' )
            
            // configure an argument
            ->addOption( 'scope', null, InputOption::VALUE_OPTIONAL, 'Run a specific handler by scope' )
            ->addOption( 'tag', null, InputOption::VALUE_OPTIONAL, 'Run a specific handler by tag' )
        
        ;
    }
    
    /**
     * Execute sync
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute( InputInterface $input, OutputInterface $output ): int {
        $this->consoleLogger->setIo( new SymfonyStyle( $input, $output ) );
        $this->consoleLogger->setCommand( $this );
        $scope = $input->getOption( 'scope' );
        $tag   = $input->getOption( 'tag' );
        
        if ( $scope && $tag ) {
            $this->io->title( 'Starting handler for ' . $scope . ':' . $tag . '...' );
        } else {
            $this->io->title( 'Starting handler...' );
        }
        
        try {
            $this->syncService->run( $scope, $tag );
            
            $this->io->success( 'Successfully ran sync' );
        } catch ( \Exception $exception ) {
            $this->io->error( $exception->getMessage() );
            $this->io->error( $exception->getFile() . ':' . $exception->getLine() );
        }
        
        return 0;
    }
    
    
}