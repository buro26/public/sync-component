<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;

use Buro26\Sync\Handler\AbstractInHandler;
use Buro26\Sync\Handler\AbstractOutHandler;
use Buro26\Sync\Handler\HandlerInInterface;
use Buro26\Sync\Handler\HandlerOutInterface;

class Context implements ContextInterface {
    
    public function __construct(
        protected readonly AbstractInHandler|AbstractOutHandler $handler,
        protected readonly \Swift\HttpFoundation\ParameterBag   $attributes = new \Swift\HttpFoundation\ParameterBag( [] ),
    ) {
    }
    
    public function getHandlerType(): \Buro26\Sync\Handler\HandlerType {
        return $this->handler instanceof \Buro26\Sync\Handler\AbstractInHandler ? \Buro26\Sync\Handler\HandlerType::IN : \Buro26\Sync\Handler\HandlerType::OUT;
    }
    
    /**
     * @inheritDoc
     */
    public function getTag(): string|null {
        return $this->handler->_getConfig()?->getTag() ?? null;
    }
    
    /**
     * @inheritDoc
     */
    public function getScope(): string|null {
        return $this->handler->_getConfig()?->getScope() ?? null;
    }
    
    /**
     * @inheritDoc
     */
    public function getInTag(): string|null {
        return $this->handler->_getConfig()?->getInTag() ?? null;
    }
    
    /**
     * @inheritDoc
     */
    public function getInScope(): string|null {
        return $this->handler->_getConfig()?->getInScope() ?? null;
    }
    
    /**
     * @inheritDoc
     */
    public function getOutTag(): string|null {
        return $this->handler->_getConfig()?->getOutTag() ?? null;
    }
    
    /**
     * @inheritDoc
     */
    public function getOutScope(): string|null {
        return $this->handler->_getConfig()?->getOutScope() ?? null;
    }
    
    public function getAttributes(): \Swift\HttpFoundation\ParameterBag {
        return $this->attributes;
    }
    
    public function getHandler(): \Buro26\Sync\Handler\HandlerInInterface|\Buro26\Sync\Handler\HandlerOutInterface {
        return $this->handler;
    }
    
}