<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;


use Buro26\Sync\Middleware\Payload\PayloadInterface;

class MiddlewareHandler implements MiddlewareHandlerInterface {
    
    public function __construct(
        protected \Closure $closure,
    ) {
    }
    
    public function handle( \Buro26\Sync\Middleware\Payload\PayloadInterface $payload ): \Buro26\Sync\Middleware\Payload\PayloadInterface {
        $closure = $this->closure;
        return $closure( $payload );
    }
    
}