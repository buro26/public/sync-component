<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;

use Buro26\Sync\Handler\HandlerType;

interface ContextInterface {
    
    public function getHandlerType(): HandlerType;
    
    /**
     * Tag on which the middleware is called
     * Resolves the tag for IN handlers
     *
     * @return string|null
     */
    public function getTag(): string|null;
    
    /**
     * Scope on which the middleware is called
     * Resolves the scope for IN handlers
     *
     * @return string|null
     */
    public function getScope(): string|null;
    
    /**
     * In Tag on which the middleware is called
     * Resolves the in tag for OUT handlers
     *
     * @return string|null
     */
    public function getInTag(): string|null;
    
    /**
     * In Scope on which the middleware is called
     * Resolves the in scope for OUT handlers
     *
     * @return string|null
     */
    public function getInScope(): string|null;
    
    /**
     * Out Tag on which the middleware is called
     * Resolves the out tag for IN handlers
     *
     * @return string|null
     */
    public function getOutTag(): string|null;
    
    /**
     * Out Scope on which the middleware is called
     * Resolves the out scope for IN handlers
     *
     * @return string|null
     */
    public function getOutScope(): string|null;
    
    /**
     * Stores the attributes of the context
     * Attributes are used to store data between middleware
     *
     * @return \Swift\HttpFoundation\ParameterBag
     */
    public function getAttributes(): \Swift\HttpFoundation\ParameterBag;
    
    public function getHandler(): \Buro26\Sync\Handler\HandlerInInterface|\Buro26\Sync\Handler\HandlerOutInterface;
    
}