<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware\Payload;

use Buro26\Sync\Middleware\ContextInterface;

class Payload implements PayloadInterface {
    
    public function __construct(
        protected readonly ContextInterface  $context,
        protected readonly mixed             $body,
    ) {
    }
    
    public function getContext(): ContextInterface {
        return $this->context;
    }
    
    public function getBody(): mixed {
        return $this->body;
    }
    
    public function withBody( $body ): static {
        return new static( $this->context, $body );
    }
    
}