<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware\Payload;

use Buro26\Sync\Middleware\ContextInterface;

interface PayloadInterface {
    
    public function getContext(): ContextInterface;
    
    public function getBody(): mixed;
    
    public function withBody( $body ): static;
    
}