<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;

use Swift\DependencyInjection\Attributes\Autowire;

#[Autowire]
class MiddlewareFactory {
    
    protected MiddlewareRunner|null $middlewareRunner = null;
    
    public function __construct(
        #[Autowire( tag: 'sync.middleware' )] protected iterable|null $middleware = null,
    ) {
    }
    
    public function createRunner(): MiddlewareRunner {
        if ( $this->middlewareRunner ) {
            return $this->middlewareRunner;
        }
        
        $this->middlewareRunner = new MiddlewareRunner( new MiddlewareQueue( iterator_to_array( $this->middleware ?? [] ) ) );
        
        return $this->middlewareRunner;
    }
    
}