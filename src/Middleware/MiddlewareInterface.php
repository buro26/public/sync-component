<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;


use Buro26\Sync\Middleware\Payload\PayloadInterface;
use Swift\DependencyInjection\Attributes\DI;

#[DI( tags: [ 'sync.middleware' ] )]
interface MiddlewareInterface {
    
    /**
     * Order of the middleware. The middleware will be sorted by this value.
     *
     * @return int
     */
    public function getOrder(): int;
    
    /**
     * Process a given payload and return the result.
     */
    public function process( PayloadInterface $payload, MiddlewareAction $action, MiddlewareHandlerInterface $handler ): PayloadInterface;
    
}