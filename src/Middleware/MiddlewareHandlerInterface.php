<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;

interface MiddlewareHandlerInterface {
    
    public function handle( \Buro26\Sync\Middleware\Payload\PayloadInterface $payload ): \Buro26\Sync\Middleware\Payload\PayloadInterface;
    
}