<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;

enum MiddlewareAction {
    
    /**
     * Wrapped around transforming incoming items in HandlerSyncItem objects
     */
    case SYNC_ITEMS_IN;
    
    /**
     * Wrapped around processing of a single incoming item
     */
    case SYNC_ITEM_IN;
    
    /**
     * Wrapped around processing of a single outgoing item in a HandlerSyncItem object
     */
    case SYNC_ITEM_OUT_PROCESS;
    
    /**
     * Wrapped around transforming HandlerSyncItem objects into outgoing items and sending them
     */
    case SYNC_ITEM_OUT_SEND;
    
    public function is( self $action ): bool {
        return $this === $action;
    }
    
}
