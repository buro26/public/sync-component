<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;



use Buro26\Sync\Middleware\Payload\PayloadInterface;

class MiddlewareRunner {
    
    public function __construct(
        protected \Buro26\Sync\Middleware\MiddlewareQueue $queue,
    ) {
    }
    
    public function run(
        PayloadInterface $payload,
        MiddlewareAction $action,
        \Closure $finalHandler,
    ): PayloadInterface {
        $this->queue->compile();
        
        return $this->call( $payload, $action, $finalHandler, 0 );
    }
    
    protected function call(
        mixed $payload,
        MiddlewareAction $action,
        \Closure $finalHandler,
        int   $key,
    ): mixed {
        $middlewares = $this->queue;
        
        if ( ! $middlewares->has( $key ) ) {
            return $finalHandler( $payload );
        }
        
        return $middlewares->get( $key )->process(
            $payload,
            $action,
            new MiddlewareHandler( function( mixed $payload ) use ( $action, $finalHandler, $key ) {
                return $this->call( $payload, $action, $finalHandler, $key + 1 );
            } ),
        );
    }
    
}