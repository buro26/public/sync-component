<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Extraction;

use Buro26\Sync\DTO\ObjectInterface;
use Swift\DependencyInjection\Attributes\Autowire;

#[Autowire]
final class Extractor {
    
    public function __construct(
        /**
         * @var \Buro26\Sync\DTO\Extraction\ExtractionStrategyInterface[] $strategies
         */
        #[Autowire( tag: 'sync.extraction_strategy' )]
        protected readonly iterable $strategies,
    ) {
    }
    
    public function extract( ObjectInterface $dto ): \stdClass {
        $target = new \stdClass();
        
        $strategies = [];
        foreach ( $this->strategies as $strategy ) {
            if ( $strategy->supports( $dto ) ) {
                $strategies[] = $strategy;
            }
        }
        
        if ( empty( $strategies ) ) {
            return $target;
        }
        
        return $strategies[ 0 ]->extract(
            $dto,
            $target,
            $this->makeNextCall( $strategies, 1 ),
        );
    }
    
    protected function makeNextCall( array $strategies, int $key ): \Closure {
        return function( ObjectInterface $dto, \stdClass $target ) use ( $strategies, $key ): \stdClass {
            $strategy = $strategies[ $key ] ?? null;
            
            if ( ! $strategy ) {
                return $target;
            }
            
            return $strategy->extract( $dto, $target, $this->makeNextCall( $strategies, $key + 1 ) );
        };
    }
    
}