<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Extraction;

use Buro26\Sync\DTO\ObjectInterface;

class ObjectAdditionalAttributesExtractionStrategy implements ExtractionStrategyInterface {
    
    /**
     * @inheritDoc
     */
    public function supports( ObjectInterface $dto ): bool {
        // Check if sync item has additional properties attached
        return ! empty( $dto->getAdditionalAttributes() );
    }
    
    /**
     * @inheritDoc
     */
    public function extract( ObjectInterface $dto, \stdClass $target, \Closure $next ): \stdClass {
        foreach ( $dto->getAdditionalAttributes() as $attributeMethod => $attributeName ) {
            if ( ! method_exists( $dto, $attributeMethod ) ) {
                continue;
            }
            
            $target->{$attributeName} = $dto->{$attributeMethod}();
        }
        
        return $next( $dto, $target );
    }
    
}