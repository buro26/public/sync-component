<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Extraction;

use Buro26\Sync\DTO\ObjectInterface;

class ObjectFieldsExtractionStrategy implements ExtractionStrategyInterface {
    
    /**
     * @inheritDoc
     */
    public function supports( ObjectInterface $dto ): bool {
        return ! empty( $dto->getFields() );
    }
    
    /**
     * @inheritDoc
     */
    public function extract( ObjectInterface $dto, \stdClass $target, \Closure $next ): \stdClass {
        if ( ! property_exists( $target, 'fields' ) ) {
            $target->fields = [];
        }
        
        foreach ( $dto->getFields() ?? [] as $field ) {
            $fieldItem             = new \stdClass();
            $fieldItem->fieldName  = $field->getFieldName();
            $fieldItem->type       = $field->getType()->value;
            $fieldItem->fieldValue = $field->getFieldValue();
            $fieldItem->public     = $field->getIsPublic();
            $target->fields[]      = $fieldItem;
        }
        
        return $next( $dto, $target );
    }
    
}