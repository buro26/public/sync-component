<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Extraction;

use Buro26\Sync\DTO\ObjectInterface;
use Swift\DependencyInjection\Attributes\DI;

#[DI( tags: [ 'sync.extraction_strategy' ] )]
interface ExtractionStrategyInterface {
    
    /**
     * Determine if this strategy supports the given DTO
     *
     * @param \Buro26\Sync\DTO\ObjectInterface $dto
     *
     * @return bool
     */
    public function supports( ObjectInterface $dto ): bool;
    
    /**
     * Extract data from DTO object and parse to simple generic stdClass object
     *
     * @param \Buro26\Sync\DTO\ObjectInterface $dto
     * @param \stdClass                        $target
     * @param \Closure                         $next
     *
     * @return \stdClass
     */
    public function extract( ObjectInterface $dto, \stdClass $target, \Closure $next ): \stdClass;
    
}