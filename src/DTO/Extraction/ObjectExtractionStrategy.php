<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Extraction;

use Buro26\Sync\DTO\ObjectInterface;

class ObjectExtractionStrategy implements ExtractionStrategyInterface {
    
    /**
     * @inheritDoc
     */
    public function supports( ObjectInterface $dto ): bool {
        return true;
    }
    
    /**
     * @inheritDoc
     */
    public function extract( ObjectInterface $dto, \stdClass $target, \Closure $next ): \stdClass {
        $target->id = $dto->getID();
        
        return $next($dto, $target);
    }
    
}