<?php declare(strict_types=1);


namespace Buro26\Sync\DTO;

use stdClass;

abstract class AbstractObject implements ObjectInterface {

    /**
     * HandlerSyncItem constructor.
     *
     * @param stdClass|null $payload
     * @param string|null $reference
     */
    public function __construct(
        public stdClass|null $payload = null,
        public string|null $reference = null
    ) {
    }
    
    /**
     * @return \stdClass|null
     */
    public function getPayload(): stdClass|null {
        return $this->payload;
    }
    
    /**
     * @return string|null
     */
    public function getReference(): string|null {
        return $this->reference;
    }

}