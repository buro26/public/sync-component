<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO;

/**
 * Interface HandlerSyncItemInterface
 *
 * Implement class create generic output content despite non-generic input
 *
 * @package Sync\Handler
 */
interface ObjectInterface {
    
    /**
     * Optionally add extra attribute calls by added the method call to the class and referring to this in the return array
     *
     * For example ['getAdditionalAttributes' => 'attributeName']
     *
     * @return array|null
     */
    public function getAdditionalAttributes(): array|null;
    
    /**
     * @return int|string
     */
    public function getID(): int|string;
    
    /**
     * Get (optional) fields
     *
     * @return \Buro26\Sync\DTO\Field\FieldInterface[]|null
     */
    public function getFields(): array|null;
    
}