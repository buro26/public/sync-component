<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Field;

/**
 * Class HandlerSyncItemFieldTypes
 *
 * Predefined list of field types. By using this list the client can easily determine how to deal with the field value.
 *
 * @package Sync\Handler
 */
enum FieldType: string {
    
    // Convert constants to cases
    case TEXT = 'text';
    case HTML = 'html';
    case JSON = 'json';
    case NUMBER = 'number';
    case FLOAT = 'float';
    case BOOL = 'bool';
    case DATETIME = 'datetime';
    
}