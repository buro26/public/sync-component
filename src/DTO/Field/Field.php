<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Field;


use Swift\DependencyInjection\Attributes\DI;

#[DI( autowire: false )]
class Field implements FieldInterface {
    
    public function __construct(
        protected string    $fieldName,
        protected FieldType $type,
        protected mixed     $fieldValue,
        protected bool      $public = true,
    ) {
    }
    
    /**
     * Named identifier for this field
     *
     * @return string
     */
    public function getFieldName(): string {
        return $this->fieldName;
    }
    
    /**
     * Typing of the field value
     *
     * @return \Buro26\Sync\DTO\Field\FieldType type of the field (e.g. 'integer', 'array', etc.)
     */
    public function getType(): FieldType {
        return $this->type;
    }
    
    /**
     * Raw value of the field
     *
     * @return mixed
     */
    public function getFieldValue(): mixed {
        return $this->fieldValue;
    }
    
    /**
     * Get is public
     *
     * @return bool
     */
    public function getIsPublic(): bool {
        return $this->public;
    }
}