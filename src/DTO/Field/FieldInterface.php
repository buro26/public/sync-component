<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Field;


interface FieldInterface {
    
    /**
     * Named identifier for this field
     *
     * @return string
     */
    public function getFieldName(): string;
    
    /**
     * Typing of the field value
     *
     * @return \Buro26\Sync\DTO\Field\FieldType type of the field (e.g. 'integer', 'array', etc.)
     */
    public function getType(): FieldType;
    
    /**
     * Raw value of the field
     *
     * @return mixed
     */
    public function getFieldValue(): mixed;
    
    /**
     * Whether the field is public
     *
     * @return bool
     */
    public function getIsPublic(): bool;
    
}