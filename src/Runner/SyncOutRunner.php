<?php declare( strict_types=1 );

namespace Buro26\Sync\Runner;


use Swift\DependencyInjection\Attributes\Autowire;
use Swift\Orm\Collection\ArrayCollection;

#[Autowire]
final class SyncOutRunner implements SyncRunnerInterface {
    
    public function __construct(
        private readonly \Swift\Orm\EntityManager                  $entityManager,
        private readonly \Swift\Logging\AppLogger                  $logger,
        private readonly \Buro26\Sync\Middleware\MiddlewareFactory $middlewareFactory,
        private readonly \Buro26\Sync\DTO\Extraction\Extractor     $extractor,
    ) {
    }
    
    public function supports( \Buro26\Sync\Handler\HandlerType $handlerType ): bool {
        return $handlerType->is( \Buro26\Sync\Handler\HandlerType::OUT );
    }
    
    public function run( \Buro26\Sync\Handler\HandlerOutInterface|\Buro26\Sync\Handler\HandlerInInterface $handler ): void {
        $config           = $handler->_getConfig();
        $middlewareRunner = $this->middlewareFactory->createRunner();
        $errors           = new ArrayCollection( [] );
        $notices          = new ArrayCollection( [] );
        
        $syncItemsIn = $this->getSyncItems( $config->getInScope(), $config->getInTag() );
        
        $syncMoment = new \DateTimeImmutable();
        
        if ( $syncItemsIn->count() < 1 ) {
            return;
        }
        
        foreach ( $syncItemsIn as /** @var \Buro26\Sync\Entity\SyncItem $syncItemIn */ $syncItemIn ) {
            $syncOutEntity = $this->getSyncOutItem( $config->getOutScope(), $config->getOutTag(), $syncItemIn );
            
            if ( ! $this->shouldItemOut( $syncItemIn, $syncOutEntity ) ) {
                continue;
            }
            
            $result = $middlewareRunner->run(
                new \Buro26\Sync\Middleware\Payload\Payload( new \Buro26\Sync\Middleware\Context( $handler ), $syncOutEntity ),
                \Buro26\Sync\Middleware\MiddlewareAction::SYNC_ITEM_OUT_PROCESS,
                static function( \Buro26\Sync\Middleware\Payload\PayloadInterface $payload ) use ( $handler ) {
                    return $payload->withBody( $handler->processItem( $payload->getBody() ) );
                },
            );
            /** @var \Buro26\Sync\DTO\AbstractObject $processed */
            $processed = $result->getBody();
            
            $middlewareRunner->run(
                new \Buro26\Sync\Middleware\Payload\Payload( new \Buro26\Sync\Middleware\Context( $handler ), $processed ),
                \Buro26\Sync\Middleware\MiddlewareAction::SYNC_ITEM_OUT_SEND,
                function( \Buro26\Sync\Middleware\Payload\PayloadInterface $payload ) use ( $handler, $syncOutEntity, $syncMoment, $errors, $syncItemIn, $notices ) {
                    $data = $this->extractor->extract( $payload->getBody() );
                    
                    try {
                        $handler->outItem( $data );
                        
                        $this->updateSyncOutItem( $syncOutEntity, $syncMoment );
                    } catch ( \Buro26\Sync\Exception\HandlerFailedException $exception ) {
                        // Catch fault here so others items can still be tried
                        $errors->append(
                            [
                                'reference' => $syncItemIn->getReference(),
                                'message'   => sprintf( 'Handler failed while running out sync: %s', $exception->getMessage() ),
                            ],
                        );
                    } catch ( \Buro26\Sync\Exception\HandlerNoticeException $exception ) {
                        // Catch fault here so others items can still be tried
                        $notices->append(
                            [
                                'reference' => $syncItemIn->getReference(),
                                'message'   => sprintf( 'Handler notice while running out sync: %s', $exception->getMessage() ),
                            ],
                        );
                    }
                    
                    return $payload->withBody( $syncOutEntity );
                },
            );
        }
        
        if ( $notices->count() ) {
            $this->logger->debug(
                sprintf( 'Handler notice while running out sync: %s', get_class( $handler ) ),
                $notices->getArrayCopy(),
            );
        }
        
        if ( $notices->count() ) {
            $this->logger->error(
                sprintf( 'Handler error while running out sync: %s', get_class( $handler ) ),
                $errors->getArrayCopy(),
            );
        }
    }
    
    /**
     * Get list of sync items belonging to handler
     *
     * @param string $scope
     * @param string $tag
     *
     * @return \Swift\Orm\Dbal\ResultCollectionInterface
     */
    private function getSyncItems( string $scope, string $tag ): \Swift\Orm\Dbal\ResultCollectionInterface {
        try {
            return $this->entityManager->findMany(
                \Buro26\Sync\Entity\SyncItem::class,
                [
                    'scope' => $scope,
                    'tag'   => $tag,
                ],
            );
        } catch ( \Exception $exception ) {
            throw new \Buro26\Sync\Exception\HandlerFailedException(
                $exception->getMessage(),
                [
                    'task'  => 'Sync Out Service => getSyncItems',
                    'scope' => 'scope',
                    'tag'   => 'tag',
                ],
                $exception->getCode(), $exception
            );
        }
    }
    
    /**
     * Get sync item entity, if it doesn't exist yet it will return a pre-populated object without id
     *
     * @param string                       $outScope
     * @param string                       $outTag
     * @param \Buro26\Sync\Entity\SyncItem $syncItemIn
     *
     * @return \Buro26\Sync\Entity\SyncItemOut
     */
    private function getSyncOutItem( string $outScope, string $outTag, \Buro26\Sync\Entity\SyncItem $syncItemIn ): \Buro26\Sync\Entity\SyncItemOut {
        if ( $syncItemIn->getSyncItemOut() ) {
            return $syncItemIn->getSyncItemOut();
        }
        
        $syncItemOut = new \Buro26\Sync\Entity\SyncItemOut();
        $syncItemOut->setScope( $outScope );
        $syncItemOut->setTag( $outTag );
        $syncItemOut->setSyncItem( $syncItemIn );
        
        return $syncItemOut;
    }
    
    /**
     * Update sync out item
     *
     * @param \Buro26\Sync\Entity\SyncItemOut $syncOutItem
     * @param \DateTimeInterface              $updatedAt
     *
     * @throws \Buro26\Sync\Exception\HandlerFailedException
     */
    private function updateSyncOutItem( \Buro26\Sync\Entity\SyncItemOut $syncOutItem, \DateTimeInterface $updatedAt ): void {
        $syncOutItem->setUpdated( $updatedAt );
        
        $this->entityManager->persist( $syncOutItem );
        
        try {
            $this->entityManager->run();
        } catch ( \Throwable $e ) {
            throw new \Buro26\Sync\Exception\HandlerFailedException(
                $e->getMessage(),
                [
                    'task'  => 'Sync Out Service => updateSyncOutItem',
                    'scope' => $syncOutItem->getScope(),
                    'tag'   => $syncOutItem->getTag(),
                ],
                $e->getCode(),
                $e,
            );
        }
    }
    
    public function shouldItemOut( \Buro26\Sync\Entity\SyncItem $syncItem, \Buro26\Sync\Entity\SyncItemOut $outItem ): bool {
        if ( $outItem->getId() === null || $outItem->getUpdated() === null ) {
            return true;
        }
        
        return ( $syncItem->getUpdated()->getTimestamp() >= $outItem->getUpdated()->getTimestamp() );
    }
    
}