<?php declare( strict_types=1 );


namespace Buro26\Sync\Runner;


use Buro26\Sync\Entity\SyncItem;
use Swift\DependencyInjection\Attributes\Autowire;

#[Autowire]
final class SyncInRunner implements SyncRunnerInterface {
    
    public function __construct(
        private readonly \Swift\Orm\EntityManager                     $entityManager,
        private readonly \Swift\Logging\AppLogger                     $logger,
        private readonly \Buro26\Sync\Console\ConsoleLogger           $consoleLogger,
        private readonly \Buro26\Sync\Middleware\MiddlewareFactory    $middlewareFactory,
        private readonly \Buro26\Sync\DTO\Extraction\Extractor        $extractor,
    ) {
    }
    
    public function supports( \Buro26\Sync\Handler\HandlerType $handlerType ): bool {
        return $handlerType->is( \Buro26\Sync\Handler\HandlerType::IN );
    }
    
    public function run( \Buro26\Sync\Handler\HandlerOutInterface|\Buro26\Sync\Handler\HandlerInInterface $handler ): void {
        $config = $handler->_getConfig();
        $scope  = $config?->getScope() ?? $config->getOutScope();
        $tag    = $config?->getTag() ?? $config->getOutTag();
        
        // Returns array of the current active ids
        $presentIds = $this->execute( $handler );
        
        // Check if there records on active which are not present anymore. If so; deactivate them
        $this->checkDeletions( $presentIds, $scope, $tag );
        
        $this->consoleLogger->getIo()->newLine();
    }
    
    
    /**
     * Executes a single incoming handler
     *
     * @param \Buro26\Sync\Handler\AbstractInHandler $handler
     *
     * @return array    array of present ids
     */
    public function execute( \Buro26\Sync\Handler\AbstractInHandler $handler ): array {
        $config           = $handler->_getConfig();
        $middlewareRunner = $this->middlewareFactory->createRunner();
        
        // Run middleware to process items into HandlerSyncItem objects
        $result = $middlewareRunner->run(
            new \Buro26\Sync\Middleware\Payload\Payload( new \Buro26\Sync\Middleware\Context( $handler ), $handler->getItems() ),
            \Buro26\Sync\Middleware\MiddlewareAction::SYNC_ITEMS_IN,
            static function( \Buro26\Sync\Middleware\Payload\PayloadInterface $payload ) use ( $handler ) {
                return $payload->withBody( $handler->processItems( $payload->getBody() ) );
            },
        );
        /** @var \Buro26\Sync\DTO\AbstractObject[] $processed */
        $processed = $result->getBody();
        
        /**
         * @var array $presentIds Reference ids which have been received and are still present. The will be used to check which items are not present anymore
         */
        $presentIds = [];
        
        foreach ( $processed as $item ) {
            // Add item reference to present ids
            $presentIds[] = $item->reference;
            
            $middlewareRunner->run(
                new \Buro26\Sync\Middleware\Payload\Payload( new \Buro26\Sync\Middleware\Context( $handler ), $item ),
                \Buro26\Sync\Middleware\MiddlewareAction::SYNC_ITEM_IN,
                function( \Buro26\Sync\Middleware\Payload\PayloadInterface $middlewarePayload ) use ( $config, $handler ) {
                    $item = $middlewarePayload->getBody();
                    
                    // Get object with all possible properties
                    $syncItemData = $this->entityManager->findOne(
                        SyncItem::class,
                        [
                            'reference' => $item->reference,
                            'scope'     => $config->getScope(),
                            'tag'       => $config->getTag(),
                        ],
                    );
                    
                    if ( ! $syncItemData ) {
                        $syncItemData = ( new SyncItem() )
                            ->setReference( $item->getReference() )
                            ->setScope( $config->getScope() )
                            ->setTag( $config->getTag() );
                    }
                    
                    $payload = $this->extractor->extract( $item );
                    
                    if ( $syncItemData->getId() && ( $syncItemData->getValue() == $payload ) && ( $syncItemData->getState() > 0 ) ) {
                        // Item already exists, data has not been changed, and it's already marked as active.
                        // This means nothing to do, let's move on
                        return $middlewarePayload->withBody( $syncItemData );
                    }
                    
                    $syncItemData->setValue( $payload )
                                 ->setState( 1 );
                    
                    // Save the item (when the id property is null this will create a new record)
                    $this->entityManager->persist( $syncItemData );
                    
                    try {
                        $this->entityManager->run();
                    } catch ( \Throwable $e ) {
                        $this->consoleLogger->getIo()?->error( sprintf( 'Handler error while running sync: %s', $e->getMessage() ) );
                        $this->logger->error(
                            sprintf( 'Handler error while running sync: %s', $e->getMessage() ),
                            [
                                'handler' => get_class( $handler ),
                                'scope'   => $config->getScope(),
                                'tag'     => $config->getTag(),
                                'context' => null,
                            ],
                        );
                    }
                    
                    return $middlewarePayload->withBody( $syncItemData );
                },
            );
        }
        
        $this->consoleLogger->getIo()->writeln( sprintf( 'Synced %d items', count( $processed ) ) );
        
        return $presentIds;
    }
    
    /**
     * Check if there records on active which are not present anymore. If so; deactivate them
     *
     * @param array  $presentIDs
     * @param string $scope
     * @param string $tag
     */
    protected function checkDeletions( array $presentIDs, string $scope, string $tag ): void {
        $list = $this->entityManager->findMany(
            SyncItem::class,
            [
                'scope' => $scope,
                'tag'   => $tag,
                'state' => 1,
            ],
        );
        
        if ( $list->count() < 1 ) {
            $this->consoleLogger->getIo()->writeln( 'No items to deactivate' );
            
            return;
        }
        
        $numDeactivated = 0;
        foreach ( $list as $item ) {
            /** @var SyncItem $item */
            if ( in_array( $item->getReference(), $presentIDs, false ) ) {
                // Item is still present so all is good
                continue;
            }
            
            // If we make it here the item has not been received. This means we should disable it.
            $item->setState( 0 );
            
            $this->entityManager->persist( $item );
        }
        
        $this->entityManager->run();
        
        $this->consoleLogger->getIo()->writeln( sprintf( 'Deactivated %d items', $numDeactivated ) );
    }
    
}