<?php declare( strict_types=1 );


namespace Buro26\Sync\Runner;

use Buro26\Sync\Handler\HandlerType;
use Swift\DependencyInjection\Attributes\DI;

/**
 * Sync Runner is responsible for running the actual sync process for the supported handler type
 */
#[DI( tags: [ 'sync.runner' ] )]
interface SyncRunnerInterface {
    
    /**
     * Whether this runner supports the given handler type
     *
     * @param \Buro26\Sync\Handler\HandlerType $handlerType
     *
     * @return bool
     */
    public function supports( HandlerType $handlerType ): bool;
    
    public function run( \Buro26\Sync\Handler\HandlerInInterface|\Buro26\Sync\Handler\HandlerOutInterface $handler ): void;
    
}