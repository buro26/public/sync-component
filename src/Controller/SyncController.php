<?php declare( strict_types=1 );


namespace Buro26\Sync\Controller;


use Swift\Controller\AbstractController;
use Swift\HttpFoundation\JsonResponse;
use Swift\Router\Attributes\Route;
use Swift\Router\Exceptions\InternalErrorException;
use Swift\Router\RouteParameterBag;
use Swift\Router\Types\RouteMethod;
use Buro26\Sync\Service\SyncService;

#[Route( method: RouteMethod::GET, route: '/sync/', name: 'sync' )]
final class SyncController extends AbstractController {
    
    public function __construct(
        private readonly SyncService $syncService
    ) {
    }
    
    /**
     * @param \Swift\Router\RouteParameterBag $params
     *
     * @return JSONResponse
     */
    #[Route( method: RouteMethod::GET, route: '/latest/[a:scope]/[a:tag]/', name: 'sync.latest' )]
    public function getLatest( RouteParameterBag $params ): JsonResponse {
        parse_str( $this->getRequest()->getUri()->getQuery(), $query );
        
        $fromValue = $query[ 'from' ] ?? null;
        $toValue   = $query[ 'to' ] ?? null;
        
        $from = $fromValue ? strtotime( urldecode( $fromValue ) ) : null;
        $to   = $toValue ? strtotime( urldecode( $toValue ) ) : null;
        
        try {
            $items = $this->syncService->getItems(
                $params->get( 'scope' )?->getValue(),
                $params->get( 'tag' )?->getValue(),
                $from,
                $to
            );
            
            return new JsonResponse( $items );
        } catch ( \Exception $exception ) {
            throw new InternalErrorException( $exception->getMessage() );
        }
    }
    
    /**
     * @param \Swift\Router\RouteParameterBag $params
     *
     * @return JSONResponse
     */
    #[Route( method: RouteMethod::GET, route: '/run/', name: 'sync.run' )]
    public function runSync( RouteParameterBag $params ): JSONResponse {
        $this->syncService->run();
        
        return new JSONResponse( [] );
    }
    
}