<?php declare( strict_types=1 );


namespace Buro26\Sync\Exception;


use RuntimeException;
use Throwable;

class HandlerFailedException extends RuntimeException {
    
    /**
     * @var mixed $context
     */
    private mixed $context;
    
    public function __construct( string $message = "", $context = null, int $code = 0, Throwable $previous = null ) {
        $this->context = $context;
        
        parent::__construct( $message, $code, $previous );
    }
    
    /**
     * @return mixed
     */
    public function getContext(): mixed {
        return $this->context;
    }
    
}