<?php declare( strict_types=1 );


namespace Buro26\Sync\Handler;



use Buro26\Sync\Attributes\OutHandler;
use Buro26\Sync\DTO\ObjectInterface;
use Buro26\Sync\Entity\SyncItemOut;
use Buro26\Sync\Exception\HandlerFailedException;

abstract class AbstractOutHandler implements HandlerOutInterface {
    
    protected readonly OutHandler $outHandler;
    
    public function _getConfig(): OutHandler {
        return $this->outHandler;
    }
    
    public function _setConfig( OutHandler $outHandler ): void {
        $this->outHandler = $outHandler;
    }
    
    /**
     * Process given entity of sync item into specific format in an instance of HandlerSyncItemInterface
     *
     * @param \Buro26\Sync\Entity\SyncItemOut $item
     *
     * @return ObjectInterface
     * @throws \Buro26\Sync\Exception\HandlerFailedException
     */
    abstract public function processItem( SyncItemOut $item ): ObjectInterface;
    
    /**
     * Write item to external source
     *
     * @param \stdClass $item
     *
     * @throws HandlerFailedException
     */
    abstract public function outItem( \stdClass $item ): void;

}