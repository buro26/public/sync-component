<?php declare( strict_types=1 );


namespace Buro26\Sync\Handler;

use Buro26\Sync\Attributes\InHandler;
use Buro26\Sync\Attributes\OutHandler;
use Swift\DependencyInjection\Attributes\Autowire;

#[Autowire]
final class HandlerFactory {
    
    /** @var \Buro26\Sync\Handler\HandlerInInterface[] */
    private array $inHandlers;
    /** @var \Buro26\Sync\Handler\HandlerOutInterface[] */
    private array $outHandlers;
    
    public function __construct(
        private readonly \Swift\Code\AttributeReader         $attributeReader,
        #[Autowire( tag: 'sync.handler.in' )] iterable|null  $inHandlers,
        #[Autowire( tag: 'sync.handler.out' )] iterable|null $outHandlers,
    ) {
        foreach ( $inHandlers as $handler ) {
            /** @var \Buro26\Sync\Attributes\InHandler $attr */
            $attr = $this->attributeReader->getClassAnnotation( new \ReflectionClass( $handler ), InHandler::class );
            
            if ( ! $handler instanceof HandlerInInterface ) {
                throw new \RuntimeException( 'Handler ' . $handler::class . ' must implement \Buro26\Sync\Handler\HandlerInInterface' );
            }
            
            if ( ! $attr instanceof InHandler ) {
                throw new \RuntimeException( 'Handler ' . $handler::class . ' must have a \Buro26\Sync\Attributes\InHandler attribute' );
            }
            
            $handler->_setConfig( $attr );
            
            $this->inHandlers[ $handler::class ] = $handler;
        }
        
        foreach ( $outHandlers as $handler ) {
            /** @var \Buro26\Sync\Attributes\InHandler $attr */
            $attr = $this->attributeReader->getClassAnnotation( new \ReflectionClass( $handler ), OutHandler::class );
            
            if ( ! $handler instanceof HandlerOutInterface ) {
                throw new \RuntimeException( 'Handler ' . $handler::class . ' must implement \Buro26\Sync\Handler\HandlerOutInterface' );
            }
            
            if ( ! $attr instanceof OutHandler ) {
                throw new \RuntimeException( 'Handler ' . $handler::class . ' must have a \Buro26\Sync\Attributes\OutHandler attribute' );
            }
            
            $handler->_setConfig( $attr );
            
            $this->outHandlers[ $handler::class ] = $handler;
        }
    }
    
    public function getHandlers( HandlerType $type, string|null $scope = null, string|null $tag = null ): array {
        $handlers = $type === HandlerType::IN ? $this->inHandlers : $this->outHandlers;
        
        $getAll = static function() use ( $handlers ): array {
            $registeredHandlers = [];
            
            foreach ( $handlers as $handler ) {
                $registeredHandlers[] = $handler;
            }
            
            return $registeredHandlers;
        };
        
        
        return match ( true ) {
            ( $scope !== null && $tag !== null ) => $this->getHandlersForScopeAndTag( $type, $scope, $tag ),
            ( $scope !== null ) => $this->getHandlersForScope( $type, $scope ),
            ( $tag !== null ) => $this->getHandlersForTag( $type, $tag ),
            default => $getAll(),
        };
    }
    
    public function getHandlersForScope( HandlerType $type, string $scope ): array {
        $handlers = $type === HandlerType::IN ? $this->inHandlers : $this->outHandlers;
        $wanted   = [];
        foreach ( $handlers as $handler ) {
            if ( $handler->_getConfig()->getIdentifier() !== $scope ) {
                continue;
            }
            
            $wanted[] = $handler;
        }
        
        return $wanted;
    }
    
    public function getHandlersForTag( HandlerType $type, string $tag ): array {
        $handlers = $type === HandlerType::IN ? $this->inHandlers : $this->outHandlers;
        $wanted   = [];
        foreach ( $handlers as $handler ) {
            if ( $handler->_getConfig()->getTag() !== $tag ) {
                continue;
            }
            
            $wanted[] = $handler;
        }
        
        return $wanted;
    }
    
    public function getHandlersForScopeAndTag( HandlerType $type, string $scope, string $tag ): array {
        $handlers = $type === HandlerType::IN ? $this->inHandlers : $this->outHandlers;
        $wanted   = [];
        foreach ( $handlers as $handler ) {
            if ( ( $handler->_getConfig()->getScope() !== $scope ) || ( $handler->_getConfig()->getTag() !== $tag ) ) {
                continue;
            }
            
            $wanted[] = $handler;
        }
        
        return $wanted;
    }
    
}