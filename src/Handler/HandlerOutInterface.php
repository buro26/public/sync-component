<?php declare( strict_types=1 );


namespace Buro26\Sync\Handler;

use Buro26\Sync\DTO\ObjectInterface;
use Buro26\Sync\Entity\SyncItemOut;
use Buro26\Sync\Exception\HandlerFailedException;
use Swift\DependencyInjection\Attributes\DI;


#[DI(tags: ['sync.handler.out'])]
interface HandlerOutInterface {
    
    /**
     * Process given entity of sync item into specific format in an instance of HandlerSyncItemInterface
     *
     * @param \Buro26\Sync\Entity\SyncItemOut $item
     *
     * @return ObjectInterface
     * @throws \Buro26\Sync\Exception\HandlerFailedException
     */
    public function processItem( SyncItemOut $item ): ObjectInterface;
    
    /**
     * Write item to external source
     *
     * @param \stdClass $item
     *
     * @throws HandlerFailedException
     */
    public function outItem( \stdClass $item ): void;
    
}