<?php declare( strict_types=1 );


namespace Buro26\Sync\Handler;

enum HandlerType {
    
    case IN;
    case OUT;
    
    public function is( self $type ): bool {
        return $this === $type;
    }
    
}
