<?php declare( strict_types=1 );


namespace Buro26\Sync\Handler;

use Buro26\Sync\DTO\ObjectInterface;
use Buro26\Sync\Exception\HandlerFailedException;
use Swift\DependencyInjection\Attributes\DI;

#[DI(tags: ['sync.handler.in'])]
interface HandlerInInterface {
    
    /**
     * @return array|null    Get current list of items. No need to check for deletions, creations, etc.
     *
     * @throws HandlerFailedException
     */
    public function getItems(): array|null;
    
    /**
     * @param array $items
     *
     * @return ObjectInterface[]|null   Please mind all items need to be an instance of HandlerSyncItem, where the data is
     *                                           the 'payload' and the 'reference' is the external id which will be use to match
     *                                           old- and new data
     *
     * @throws HandlerFailedException
     */
    public function processItems( array $items ): array|null;
    
}