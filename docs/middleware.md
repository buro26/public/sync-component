# Middleware

The middleware is used in the sync process to manipulate or validate the data. The middleware is executed in the provided order. The middleware should implement the `Buro26\Sync\Middleware\MiddlewareInterface` interface.

The middleware is always executed for a predefined action. The action is passed to the middleware as a parameter. The action is defined in the `Buro26\Sync\Middleware\MiddlewareAction` enum. The action can be one of the following:

| Action                                    | Description                                                                              | Payload                                       | Result                                          |
|-------------------------------------------|------------------------------------------------------------------------------------------|-----------------------------------------------|-------------------------------------------------|
| `MiddlewareAction::SYNC_ITEMS_IN`         | Wrapped around transforming incoming items in HandlerSyncItem objects                    | `mixed[]`                                     | `Buro26\Sync\Handler\AbstractHandlerSyncItem[]` |
| `MiddlewareAction::SYNC_ITEM_IN`          | Wrapped around processing of a single incoming item                                      | `Buro26\Sync\Handler\AbstractHandlerSyncItem` | `Buro26\Sync\Entity\SyncItem[]`                 |
| `MiddlewareAction::SYNC_ITEM_OUT_PROCESS` | Wrapped around processing of a single outgoing item in a HandlerSyncItem object          | `mixed`                                       | `Buro26\Sync\Handler\AbstractHandlerSyncItem`   |
| `MiddlewareAction::SYNC_ITEM_OUT_SEND`    | Wrapped around transforming HandlerSyncItem objects into outgoing items and sending them | `Buro26\Sync\Handler\AbstractHandlerSyncItem` | `Buro26\Sync\Entity\SyncOutItem[]`              |

> Note that all middlewares all called on every action, so it's important to check if the action is the one you want to handle. If not, you should call the next middleware in the chain as below.

```php
$handler->handle( $payload );
```

> The higher the order, the later the middleware is executed.

## Creating a Middleware
A middleware should implement the `Buro26\Sync\Middleware\MiddlewareInterface` interface. This interface has two methods:
- `getOrder()` - This method should return the order of the middleware. The middleware with the lowest order is executed first.
- `process()` - This method is called when the middleware is executed. The method receives the payload, the action and the handler as parameters. The method should return the result of the middleware. The result should be a `PayloadInterface` object.

## Payload
The payload is an object that contains the data that is passed to the middleware. The payload implements the `Buro26\Sync\Middleware\Payload\PayloadInterface` interface. This interface has three methods:
- `getBody()` - This method returns the body of the payload. The body can be any type.
- `withBody()` - This method returns a new payload with the provided body. The body can be any type.
- `getContext()` - This method returns the context of the payload. The context implements the `Buro26\Sync\Middleware\ContextInterface` interface.

## Context
The context is an object that contains the context of the payload. The context implements the `Buro26\Sync\Middleware\ContextInterface` interface.

The context contains the data about what is being synced. 

### Context Attributes
The context has an attribute bag. This bag can be used to store data between middleware. The attribute bag is an instance of `\Swift\HttpFoundation\ParameterBag`.

### ContextInterface
```php
<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware;

use Buro26\Sync\Handler\HandlerType;

interface ContextInterface {
    
    public function getHandlerType(): HandlerType;
    
    /**
     * Tag on which the middleware is called
     * Resolves the tag for IN handlers
     *
     * @return string|null
     */
    public function getTag(): string|null;
    
    /**
     * Scope on which the middleware is called
     * Resolves the scope for IN handlers
     *
     * @return string|null
     */
    public function getScope(): string|null;
    
    /**
     * In Tag on which the middleware is called
     * Resolves the in tag for OUT handlers
     *
     * @return string|null
     */
    public function getInTag(): string|null;
    
    /**
     * In Scope on which the middleware is called
     * Resolves the in scope for OUT handlers
     *
     * @return string|null
     */
    public function getInScope(): string|null;
    
    /**
     * Out Tag on which the middleware is called
     * Resolves the out tag for IN handlers
     *
     * @return string|null
     */
    public function getOutTag(): string|null;
    
    /**
     * Out Scope on which the middleware is called
     * Resolves the out scope for IN handlers
     *
     * @return string|null
     */
    public function getOutScope(): string|null;
    
    /**
     * Stores the attributes of the context
     * Attributes are used to store data between middleware
     * 
     * @return \Swift\HttpFoundation\ParameterBag
     */
    public function getAttributes(): \Swift\HttpFoundation\ParameterBag;
    
    public function getHandler(): \Buro26\Sync\Handler\HandlerInInterface|\Buro26\Sync\Handler\HandlerOutInterface;
    
}
```

## Example Middleware

```php
<?php declare( strict_types=1 );


namespace Buro26\Sync\Middleware\CoreMiddleware;

use Buro26\Sync\Middleware\MiddlewareAction;
use Buro26\Sync\Middleware\MiddlewareHandlerInterface;
use Buro26\Sync\Middleware\MiddlewareOrder;
use Buro26\Sync\Middleware\Payload\PayloadInterface;

class SyncInItemsMiddleware implements \Buro26\Sync\Middleware\MiddlewareInterface {
    
    /**
     * @inheritDoc
     */
    public function getOrder(): int {
        return MiddlewareOrder::SYNC_IN_ITEMS;
    }
    
    /**
     * @inheritDoc
     */
    public function process( PayloadInterface $payload, MiddlewareAction $action, MiddlewareHandlerInterface $handler ): mixed {
        if ( ! $action->is( MiddlewareAction::SYNC_ITEMS_IN ) ) {
            return $handler->handle( $payload );
        }
        
        $payloadBody = $payload->getBody();
        // ... do something with the payload body
        
        $result = $handler->handle( $payload->withPayload( $payloadContent ) );
        
        $payloadBody = $result->getBody();
        // ... do something with the result body
        
        return $result->withBody( $payloadBody );
    }
    
}
```