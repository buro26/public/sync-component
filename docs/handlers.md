# Handlers

Handlers are classes that are responsible for gathering data and formatting this in 'HandlerSyncItems'. Those items follow a generic interface. Based on this data the Sync Component will determine whether there are changes and if so, will save the updated data to the database and register the update.

### Types of Handlers

There are two types of handlers, InHandlers and OutHandlers. InHandlers are responsible for fetching data from an external source. OutHandlers are responsible for pushing data to an external source.

### Scope and Tag

The scope and tag are used to identify the handler. The scope is used to identify the system the handler is responsible for. The tag is used to identify the type of data the handler is responsible for.

### InHandler

A InHandler should extend from the `\Buro26\Sync\Handler\AbstractInHandler` class and register itself using the `\Buro26\Sync\Attributes\InHandler` attribute.

A InHandler has two methods, getItems() which should return an array of items fetching from the external source and processItems() which should return an array of Data Transfer Objects.

### Example InHandler

```php
<?php declare( strict_types=1 );


namespace Combipac\Handler;


use Buro26\Sync\Attributes\InHandler;
use Combipac\Helper\MediaHelper;
use Combipac\Service\ProductService;
use RuntimeException;
use Swift\DependencyInjection\Attributes\Autowire;
use Swift\Http\Request\Exceptions\RequestFailed;
use Buro26\Sync\Exception\HandlerFailedException;
use Buro26\Sync\Handler\AbstractInHandler;

#[Autowire]
#[InHandler( scope: 'combipac', tag: 'product' )]
final class ProductHandler extends AbstractInHandler {
    
    public function __construct(
        private readonly ProductService $productService,
        private readonly MediaHelper $mediaHelper
    ) {
    }
    
    /**
     * @return array|null    Get current list of items. No need to check for deletions, creations, etc.
     *                       Please mind all items need to be an instance of HandlerSyncItem, where the data is the 'payload' and the 'reference' is the external
     *                       id which will be use to match old- and new data
     * @throws HandlerFailedException
     */
    public function getItems( array|null $data = null ): array|null {
        try {
            return $this->productService->getProducts();
        } catch ( RequestFailed $exception ) {
            throw new HandlerFailedException( $exception->getMessage(), $exception->getReponse(), $exception->getCode() );
        } catch ( RuntimeException $exception ) {
            throw new HandlerFailedException( $exception->getMessage(), null, $exception->getCode() );
        }
    }
    
    public function processItems( array $items ): array|null {
        $itemsReady = [];
        foreach ( $items as $key => $item ) {
            $itemsReady[] = new ProductHandlerSyncItem( $this->mediaHelper, $item, $key );
        }
        
        return $itemsReady;
    }
}
```

### OutHandler

A OutHandler should extend from the `\Buro26\Sync\Handler\AbstractOutHandler` class and register itself using the `\Buro26\Sync\Attributes\OutHandler` attribute.

A OutHandler has two methods, processItem() which should return an instance of ObjectInterface and outItem() which should push the data to the external source.

An OutHandler has its own outScope and outTag. This is the scope and tag of the external source. It also has an inScope and inTag. This is the scope and tag of the internal source.

#### Example OutHandler

```php
<?php declare( strict_types=1 );

namespace Magento2\Handler;

use Buro26\Sync\Attributes\OutHandler;
use Buro26\Sync\Entity\SyncItemOut;
use Buro26\Sync\Handler\AbstractOutHandler;
use Magento2\Service\CustomerService;
use RuntimeException;
use Swift\DependencyInjection\Attributes\Autowire;
use Buro26\Sync\Exception\HandlerFailedException;
use Buro26\Sync\DTO\ObjectInterface;
use Swift\Http\Request\Exceptions\RequestFailed;

#[Autowire]
#[OutHandler( inScope: 'multivers', inTag: 'customer', outScope: 'magento2', outTag: 'customer' )]
final class CustomerHandler extends AbstractOutHandler {
    
    public function __construct(
        protected readonly CustomerService $customerService,
    ) {
    }
    
    /**
     * Process given entity of sync item into specific format in an instance of HandlerSyncItemInterface
     *
     * @param \Buro26\Sync\Entity\SyncItemOut $item
     *
     * @return \Buro26\Sync\DTO\ObjectInterface
     */
    public function processItem( SyncItemOut $item ): ObjectInterface {
        return new CustomerHandlerSyncItem( $item, null );
    }
    
    /**
     * Write item to external source
     *
     * @param \stdClass $item
     *
     * @throws HandlerFailedException
     */
    public function outItem( \stdClass $item ): void {
        unset( $item->fields ); // Remove fields which should not be submitted
        
        try {
            $this->customerService->updateCustomer( (int) $item->id, $item );
        } catch ( RequestFailed $exception ) {
            throw new HandlerFailedException( $exception->getReponse()->raw_body, $item, $exception->getCode() );
        } catch ( RuntimeException $exception ) {
            throw new HandlerFailedException( $exception->getMessage(), null, $exception->getCode() );
        }
    }
    
    
}
```

## Data transfer objects

A data transfer object is the object is used to determine whether there are changes and if so, will save the updated data to the database and register the update.

A handler sync item should implement the `\Buro26\Sync\DTO\ObjectInterface`, or more easily and recommended, extend from the `\Buro26\Sync\DTO\AbstractObject` class.

Handler sync items are used to map the data fetched in the handler to a generic interface that can be interpreted by the sync.

### Methods

The ObjectInterface comes with three methods that should be implemented.

#### getId

The id is used to identify the item. This is used to match old- and new data. The id should be unique for the scope and tag.

#### getAdditionalAttributes

The additional attributes adds properties to the root of the item. This is recommended for properties that should always be available, e.g. a price for a product or title for a content item.

The method should return array with the key being the name of method and the value being the name the property should have in the serialized data.

#### getFields

The fields are used to add additional properties to the item. This is recommended for properties that are not always available, e.g. a custom field for a product. Fields should be wrapped in a `\Buro26\Sync\Handler\HandlerSyncItemField` object.

Fields array added as an array on the fields property of data object.

```php
<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Field;


use Swift\DependencyInjection\Attributes\DI;

#[DI( autowire: false )]
class Field implements FieldInterface {
    
    public function __construct(
        protected string    $fieldName,
        protected FieldType $type,
        protected mixed     $fieldValue,
        protected bool      $public = true,
    ) {
    }
    
    /**
     * Named identifier for this field
     *
     * @return string
     */
    public function getFieldName(): string {
        return $this->fieldName;
    }
    
    /**
     * Typing of the field value
     *
     * @return \Buro26\Sync\DTO\Field\FieldType type of the field (e.g. 'integer', 'array', etc.)
     */
    public function getType(): FieldType {
        return $this->type;
    }
    
    /**
     * Raw value of the field
     *
     * @return mixed
     */
    public function getFieldValue(): mixed {
        return $this->fieldValue;
    }
    
    /**
     * Get is public
     *
     * @return bool
     */
    public function getIsPublic(): bool {
        return $this->public;
    }
}
```

### Example Data Transfer Object

```php
<?php declare( strict_types=1 );


namespace combipac\DTO;

use Buro26\Sync\DTO\AbstractObject;
use Buro26\Sync\DTO\Field\Field;
use Buro26\Sync\DTO\Field\FieldType;
use Combipac\Helper\MediaHelper;
use Combipac\Helper\UriHelper;

final class ProductDto extends AbstractObject {
    
    public function __construct(
        private readonly MediaHelper $mediaHelper,
        \stdClass|null               $payload = null,
        string|int|null              $reference = null,
    ) {
        
        parent::__construct( $payload, (string) $reference );
    }
    
    public function getID(): int {
        return (int) $this->payload->productId;
    }
    
    /**
     * Optionally add extra attribute calls by added the method call to the class and referring to this in the return array
     *
     * For example ['getAdditionalAttributes' => 'attributeName']
     *
     * @return array|null
     */
    public function getAdditionalAttributes(): array|null {
        return [
            'getTitle'      => 'title',
            'getContent'    => 'content',
            'getCategoryID' => 'categoryID',
            'getMedia'      => 'media',
            'getSlug'       => 'slug',
        ];
    }
    
    public function getFields(): array|null {
        $fields = [];
        
        if ( ! empty( $this->payload->technical_detail ) ) {
            foreach ( $this->payload->technical_detail as $technical_detail ) {
                $fields[] = new Field(
                    $technical_detail->name,
                    FieldType::TEXT,
                    $technical_detail->value . ' ' . $technical_detail->unit,
                    true
                );
            }
        }
        
        return [
            ...$fields,
            new Field(
                'product_id',
                FieldType::NUMBER,
                $this->payload->details->productId,
                false,
            ),
            new Field(
                'product-web-short-description',
                FieldType::HTML,
                $this->payload->details->webShortDescription[ 0 ]->description ?? null,
                false,
            ),
            new Field(
                'price',
                FieldType::FLOAT,
                $this->payload->pricing->internetPrice,
                false,
            ),
            new Field(
                'stock',
                FieldType::NUMBER,
                $this->payload->stock,
                false
            ),
        ];
    }
    
    public function getContent(): string|null {
        return $this->payload->details->webFullDescription[ 0 ]->description ?? null;
    }
    
    public function getCategoryID(): int|string|null {
        return $this->payload->categoryInternalID;
    }
    
    /**
     * Get media urls
     *
     * @return array|null
     */
    public function getMedia(): array|null {
        return $this->mediaHelper->getItemMediaUrls( $this->getID() );
    }
    
    public function getSlug(): string {
        return UriHelper::getSlug( $this->getTitle(), $this->getID() );
    }
    
    public function getTitle(): string {
        return $this->payload->details->description[ 0 ]->description;
    }
    
    
}
```

## Extraction strategy
The extraction strategy is responsible for extracting the data from the DTO into a a plain object that can be serialized and compared in the sync.

The extraction strategy should implement the `\Buro26\Sync\ExtractionStrategy\ExtractionStrategyInterface` interface.

```php
<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Extraction;

use Buro26\Sync\DTO\ObjectInterface;
use Swift\DependencyInjection\Attributes\DI;

#[DI( tags: [ 'sync.extraction_strategy' ] )]
interface ExtractionStrategyInterface {
    
    /**
     * Determine if this strategy supports the given DTO
     *
     * @param \Buro26\Sync\DTO\ObjectInterface $dto
     *
     * @return bool
     */
    public function supports( ObjectInterface $dto ): bool;
    
    /**
     * Extract data from DTO object and parse to simple generic stdClass object
     *
     * @param \Buro26\Sync\DTO\ObjectInterface $dto
     * @param \stdClass                        $target
     * @param \Closure                         $next
     *
     * @return \stdClass
     */
    public function extract( ObjectInterface $dto, \stdClass $target, \Closure $next ): \stdClass;
    
}
```

### The sync comes with three extraction strategies out of the box
- ObjectExtractionStrategy - This will attract the default properties of the DTO
- ObjectFieldsExtractionStrategy - This will extract the fields of the DTO
- ObjectAdditionalAttributesExtractionStrategy - This will extract the additional attributes of the DTO

### Adding a custom extraction strategy
To add a custom extraction strategy, create a class that implements the `\Buro26\Sync\ExtractionStrategy\ExtractionStrategyInterface` interface, It will automatically be tagged with the `sync.extraction_strategy` tag.

> Note that instead of returning the target object directly, you should return the result of the next closure. The means you can also modify the result of the following strategies.

```php
<?php declare( strict_types=1 );


namespace Buro26\Sync\DTO\Extraction;

use Buro26\Sync\DTO\ObjectInterface;

class ObjectFieldsExtractionStrategy implements ExtractionStrategyInterface {
    
    /**
     * @inheritDoc
     */
    public function supports( ObjectInterface $dto ): bool {
        return ! empty( $dto->getFields() );
    }
    
    /**
     * @inheritDoc
     */
    public function extract( ObjectInterface $dto, \stdClass $target, \Closure $next ): \stdClass {
        if ( ! property_exists( $target, 'fields' ) ) {
            $target->fields = [];
        }
        
        foreach ( $dto->getFields() ?? [] as $field ) {
            $fieldItem             = new \stdClass();
            $fieldItem->fieldName  = $field->getFieldName();
            $fieldItem->type       = $field->getType()->value;
            $fieldItem->fieldValue = $field->getFieldValue();
            $fieldItem->public     = $field->getIsPublic();
            $target->fields[]      = $fieldItem;
        }
        
        $result = $next( $dto, $target );
        
        // ... Do something with the result
        
        return $result;
    }
    
}
```