# Logging

Logging can be done in two ways:
- Swift AppLogger
- Console Logger

# Swift AppLogger
The Swift AppLogger is a logger that is registered in the Swift container. This means that you can inject it in your services and use it as you would use any other logger.

## Error levels
The AppLogger supports the following error levels:
- info
- error
- critical
- alert
- emergency
- debug
- notice

> The `error` level will trigger an email to the configured logging email address.

## Usage
```php
<?php declare( strict_types=1 );


namespace Buro26\Sync\Service;


use Swift\DependencyInjection\Attributes\Autowire;

#[Autowire]
final class MyService {
    
    public function __construct(
        private readonly \Swift\Logging\AppLogger $logger,
    ) {
    }
    
    public function doSomething( array $handlers, \Buro26\Sync\Handler\HandlerType $method ): void {
        foreach ( $handlers as $handler ) {
            $config = $handler->_getConfig();
            $scope  = $config?->getScope() ?? $config->getOutScope();
            $tag    = $config?->getTag() ?? $config->getOutTag();
            
            foreach ( $this->syncRunners ?? [] as $syncRunner ) {
                try {
                   // ... some logic
                } catch ( \Buro26\Sync\Exception\HandlerFailedException $exception ) {
                    // Handler error
                    $this->logger->error(
                        sprintf( 'Handler error while running sync: %s', $exception->getMessage() ),
                        [
                            'handler' => get_class( $handler ),
                            'scope'   => $scope,
                            'tag'     => $tag,
                            'context' => $exception->getContext(),
                        ],
                    );
                    
                    $this->logger->info(
                        sprintf( 'Handler notice while running sync: %s', $exception->getMessage() ),
                        [
                            'handler' => get_class( $handler ),
                            'scope'   => $scope,
                            'tag'     => $tag,
                            'context' => $exception->getContext(),
                        ],
                    );
                }
            }
        }
    }
    
}
```

## Console Logger
The Console Logger is a logger that is registered in the Swift container. This means that you can inject it in your services and use it as you would use any other logger.

## Usage
```php
<?php declare( strict_types=1 );


namespace Buro26\Sync\Service;


use Swift\DependencyInjection\Attributes\Autowire;

#[Autowire]
final class SyncService {
    
    public function __construct(
        private readonly \Buro26\Sync\Console\ConsoleLogger $consoleLogger,
    ) {
    }
    
    public function doSomething(): void {
        $this->consoleLogger->getIo()->writeln( '<fg=blue;options=bold>Hello World</>' );
    }
    
   
}
```