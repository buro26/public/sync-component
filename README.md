# Buro26 Sync
This a sync component for Buro26 Sync Projects. It uses the [Swift](https://github.com/SwiftAPI/swift) project.

The project is used to sync data between different systems. Data for the sync is provided by 'Handler', those are classes responsible for gathering data and formatting this in 'HandlerSyncItems'. Those items follow a generic interface. Based on this data the Sync Component will determine whether there are changes and if so, will save the updated data to the database and register the update.

The component provides and API to request all updates, creations and deletions since a certain date.

## Getting Started
### Requirements
- PHP 8.1 or higher
- [Swift](https://github.com/SwiftAPI/swift) 0.2.16 or higher

### Installation
Install the component within your [Swift](https://github.com/SwiftAPI/swift) project.

```bash
composer require buro26/sync-component
```

### Register component in Swift
Register the component in the root `services.yaml` file of your project.

```yaml
services:
  _defaults:
    autowire: false
    autoconfigure: true
    public: true
  
  Foobar\:
    resource: 'app/Foobar/*'
  
  Buro26\Sync\:
    resource: 'vendor/buro26/sync-component/src/*'
```

## Handlers
[Handlers](/docs/handlers.md) are classes that are responsible for gathering data and formatting this in 'HandlerSyncItems'. Those items follow a generic interface. Based on this data the Sync Component will determine whether there are changes and if so, will save the updated data to the database and register the update.

## Middleware
[Middleware](/docs/middleware.md) is used in the sync process to manipulate or validate the data. The middleware is executed in the provided order. The middleware should implement the `Buro26\Sync\Middleware\MiddlewareInterface` interface.

## Logging
[Logging](/docs/logging.md) can be done to a log file or to the console.

## Executing the Sync
To execute the sync, you can use the `sync:run` command.

```bash
php bin/console sync:run
```

To run the sync for a specific scope and tag, you can use the `--scope` and `--tag` options.

```bash
php bin/console sync:run --scope=combipac --tag=product
```

## Listing registered handlers
To list all registered handlers, you can use the `sync:handlers:list` command.

```bash
php bin/console sync:handlers:list
```

This will show something as below.

```bash
$ bin/console sync:handlers:list

Registered 5 handlers
=====================

 ----------------------- ---------------------- ------ ----------------------------------
  scope                   tag                    type   FQCN
 ----------------------- ---------------------- ------ ----------------------------------
  combipac                category               IN     Combipac\Handler\CategoryHandler
  combipac                machine                IN     Combipac\Handler\MachineHandler
  combipac                product                IN     Combipac\Handler\ProductHandler
  multivers => magento2   customer => customer   OUT    Magento2\Handler\CustomerHandler
  compano => magento2     product => product     OUT    Magento2\Handler\ProductHandler
 ----------------------- ---------------------- ------ ----------------------------------
```

## Fetching the sync data
### REST
Requesting the sync data can be done by using the `GET /sync/latest/[a:scope]/[a:tag]/` endpoint. This endpoint accepts the following query parameters:
- from: The date from which the sync data should be returned. This should be a valid date string.
- to: The date until which the sync data should be returned. This should be a valid date string.

The endpoint will return an object with three properties:
- creations: An array of all the created items since the provided date.
- updates: An array of all the updated items since the provided date.
- deletions: An array of all the deleted items since the provided date.

### GraphQL
Requesting the sync data can be done by using the `SyncItemsLatest` query. This query accepts the following arguments:
- scope: The scope for which the sync data should be returned.
- tag: The tag for which the sync data should be returned.
- from: The date from which the sync data should be returned. This should be a valid date string.
- to: The date until which the sync data should be returned. This should be a valid date string.

The query will return an object with three properties:
- creations: An array of all the created items since the provided date.
- updates: An array of all the updated items since the provided date.
- deletions: An array of all the deleted items since the provided date.